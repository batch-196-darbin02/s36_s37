const express = require("express");

const router = express.Router();

// In fact, routes should just contain the endpoints and it should only trigger the function but it should not be where we define the logic of our function.

// The business logic of our API should be in controllers:
const userControllers = require("../controllers/userControllers");

// Check the imported userControllers:
// console.log(userControllers);

// Import auth to be able to have access and use the verify methods to act as middleware for our routes.
// Middlewares add in the route such as verify() will have access to the req,res objects.
const auth = require("../auth");

// Destructure auth to get only our methods and save it in variables:
const {verify} = auth;


/*
	Updated Route Syntax:

	router.method('/endpoit',handlerFunction)
*/

// Register
router.post('/', userControllers.registerUser);

// Post method route to get user details
// verify() is used as middleware which means our request will get through verify first before our controller
// verify() will not only check the validity of the token but also add the decoded data of the token in the request object as the req.user
router.post("/details",verify,userControllers.getUserDetails);

// Route for User Authentication
router.post('/login',userControllers.loginUser);

// Check email
router.post('/checkEmail',userControllers.checkUserByEmail);


// User enrollment
// courseId will come from the req.body
// userId will come from the req.body
router.post('/enroll',verify,userControllers.enroll);




module.exports = router;