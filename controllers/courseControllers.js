/*
	Naming convention for controllers is that is should be named after the model or documents it is concerned with.

	Controllers are functions which contain the actual business logic of our API and is triggered by a route.

	MVC : models, views, controllers. We will discover views on ReactJS.
*/

// Import the Course model so we can manipulate it and add a new course document.
const Course = require("../models/Course");


module.exports.getAllCourses = (req,res) => {

	// Use the course model to connect to our collection and retrieve our courses.
	// To be able to query into our collections we use the model connected to that collection
	// in MongoDB: db.courses.find({})
	// Model.find() returns a collection of documents that matches our criteria similar to MongoDB's .find()
	Course.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))

};


module.exports.AddCourse = (req,res) => {
	
	// console.log(req.body);

	// using the Course model, we will use its constructor to create our Course document which will follow the schema of the model and add methods for document creation.

	let newCourse = new Course ({

		name: req.body.name,
		description: req.body.description,
		price: req.body.price

	});

	// console.log(newCourse);
	// newCourse is now an object which follows the courseSchema and with additional methods from our Course constructor
	// .save method is added into our newCourse object. This will allow us to save the content of newCourse into our collection
	// db.courses.insertOne()

	// .then() allows us to process the result of a previous function/method in its own anonymous function
	// .catch() catches the error and allows us to process and send to the client
	newCourse.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))
};


module.exports.getActiveCourses = (req,res) => {

	Course.find({isActive:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
};


module.exports.getSingleCourse = (req,res) => {
	
	console.log(req.params); // req.params is an object that contain the value captured via route params
	// the field name of the req.params indicate name of the route params
	
	// How do we get the id passed as the route params?
	console.log(req.params.courseId)

	Course.findById(req.params.courseId)
	.then(result => res.send(result))
	.catch(error => res.send(error))
};


module.exports.updateCourse = (req,res) => {

	// How do we check if we can get the Id?
	console.log(req.params.courseId);

	// How do we check the update that is input?
	console.log(req.body);

	// findByIdAndUpdate - used to update documents and has 3 arguments
	// findByIdAndUpdate(<id>,{updates as an object},{new:true})

	// We can create a new object to filter update details
	// The indicated fields in the update object will be the fields update
	// Fields/property that are not part of the update will not be updated
	let update = {

		name: req.body.name,
		description: req.body.description,
		price: req.body.price

	};

	Course.findByIdAndUpdate(req.params.courseId,update,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))

};


module.exports.archiveCourse = (req,res) => {

	// console.log(req.params.courseId);

	// Update the course document to inactive for soft delete

	let update = {

		isActive: false

	};

	Course.findByIdAndUpdate(req.params.courseId,update,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))

};